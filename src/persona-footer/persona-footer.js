import { LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
        <!-- Enlace Bootstrap -->		
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div class="bg-warning py-2 mt-2 rounded">
                <h6 class="lean text-center h6">Aplicación Persona 2021 - Reyes Vasquez Daniel Alejandro</h6>
            </div>
            
        `;
    }
}

customElements.define('persona-footer', PersonaFooter)
import { LitElement, html } from 'lit-element';  
	class PersonaFichaListado extends LitElement { 

	static get properties() {
		return {
			name: {type: String},
			yearsInCompany: {type: Number},
			profile: {type: String},
			photo: {type: Object}
		};
	}

	constructor() {
		super();			
	}


	render() {
		return html`
			<!-- Enlace Boostrap -->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
			<div class="card h-100 border border-3 border-primary shadow">
				<img src="${this.photo.src}" alt="${this.photo.alt}" height="250" class="card-img-top">
				<div class="card-body">
					<h5 class="card-title h4 bg-primary rounded text-white text-center py-1"><strong>${this.name}</strong></h5>
					<p class="card-text fst-italic">${this.profile}</p>
					<p><strong>${this.yearsInCompany}</strong> años en la empresa</p>
				</div>
			<div class="card-footer">
				<button @click="${this.deletePerson}" class="btn btn-danger shadow col-6"><strong>Eliminar</strong></button>
			</div>				
		</div>
	`;
}

	deletePerson(e) {	
		console.log("deletePerson en persona-ficha-listado");
		console.log("Se va a borrar la persona de nombre " + this.name); 

		this.dispatchEvent(
			new CustomEvent("delete-person", {
					detail: {
						name: this.name
					}
				}
			)
		);
	}
}  
customElements.define('persona-ficha-listado', PersonaFichaListado)
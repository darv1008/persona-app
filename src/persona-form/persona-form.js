import { LitElement, html } from 'lit-element';  
	class PersonaForm extends LitElement {
static get properties() {
	return {			
		person: {type: Object}
	};
}

constructor() {
	super();		

	this.person = {};		
}

render() {
	return html`	
		<!-- Enlace Bootstrap -->		
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
		<div>
			<form>
				<div class="form-group mt-2">
					<label><strong>Nombre Completo</strong></label>
					<input type="text" @input="${this.updateName}" id="personFormName" class="form-control mb-3" placeholder="Nombre Completo"/>
				<div>
				<div class="form-group">
					<label><strong>Perfil</strong></label>
					<textarea @input="${this.updateProfile}" class="form-control mb-3" placeholder="Perfil" rows="5"></textarea>
				<div>
				<div class="form-group">
					<label><strong>Años en la empresa</strong></label>
					<input type="text" @input="${this.updateYearsInCompany}" class="form-control mb-3" placeholder="Años en la empresa"/>
				<div>
				<button @click="${this.goBack}" class="btn btn-primary mb-3"><strong>Atrás</strong></button>
				<button @click="${this.storePerson}" class="btn btn-success mb-3"><strong>Guardar</strong></button>
			</form>
		</div>			
	`;
}       	

goBack(e) {
	console.log("goBack");	  
	e.preventDefault();	
	this.dispatchEvent(new CustomEvent("persona-form-close",{}));
}

updateName(e) {
	console.log("updateName");
	console.log("Actualizando la propiedad name con el valor " + e.target.value);
	this.person.name = e.target.value;
}

updateProfile(e) {
	console.log("updateProfile");
	console.log("Actualizando la propiedad profile con el valor " + e.target.value);
	this.person.profile = e.target.value;
}

updateYearsInCompany(e) {
	console.log("updateYearsInCompany");
	console.log("Actualizando la propiedad yearsInCompany con el valor " + e.target.value);
	this.person.yearsInCompany = e.target.value;
}

storePerson(e) {
	console.log("storePerson");
	e.preventDefault();
	
	this.person.photo = {
		"src": "./img/persona.jpg",
		"alt": "Persona"
	};
		
	console.log("La propiedad name vale " + this.person.name);
	console.log("La propiedad profile vale " + this.person.profile);
	console.log("La propiedad yearsInCompany vale " + this.person.yearsInCompany);
		
	if(this.person.name != undefined && this.person.profile != undefined && this.person.yearsInCompany != undefined){
		this.dispatchEvent(new CustomEvent("persona-form-store",{
			detail: {
				person:  {
						name: this.person.name,
						profile: this.person.profile,
						yearsInCompany: this.person.yearsInCompany,
						photo: this.person.photo
					}
				}
			})
		);
	}else{
		alert("Para guardar una nueva Persona debe llenar los campos");
	}
}
}
customElements.define('persona-form', PersonaForm)
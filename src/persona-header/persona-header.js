import { LitElement, html } from 'lit-element';

class PersonaHeader extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
        <!-- Enlace Bootstrap -->		
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
            <div class="bg-warning py-2 rounded">
                <h1 class="text-center h1 display-5">Aplicación Persona</h1>
            </div>
            
        `;
    }
}

customElements.define('persona-header', PersonaHeader)
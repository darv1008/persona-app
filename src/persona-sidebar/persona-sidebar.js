import { LitElement, html, css } from 'lit-element';  
	class PersonaSidebar extends LitElement {
	static get properties() {
		return {			
		};
	}

	static get styles(){
		return css
		`
			strong{
				color: white;
			}
		`;
	}

	constructor() {
		super();			
	}

	render() {
		return html`		
		<link rel="stylesheet" href="./src/styles/styles.css">
			<!-- Enlace Bootstrap -->	
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">	
			<aside>
				<section>				
					<div class="mt-3 px-3">
						<button class="w-100 btn bg-success shadow-lg" @click="${this.newPerson}"><strong>+ Agregar Persona</strong></button>
					<div>				
				</section>
			</aside>
		`;
	}    

	newPerson(e) {
		console.log("newPerson en persona-sidebar");
		console.log("Se va a crear una nueva persona");
  
		this.dispatchEvent(new CustomEvent("new-person", {})); 	
	}
}
customElements.define('persona-sidebar', PersonaSidebar)
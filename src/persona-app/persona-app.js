import { LitElement, html } from 'lit-element'; 
	import '../persona-header/persona-header.js'; 
	import '../persona-main/persona-main.js'; 
	import '../persona-footer/persona-footer.js'; 
	import '../persona-sidebar/persona-sidebar.js'; 
class PersonaApp extends LitElement {
	static get properties() {
		return {			
		};
	}

	constructor() {
		super();			
	}

	render() {
		return html`
			<!-- Enlace Bootstrap -->		
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
			<persona-header></persona-header>
			<div class="row">
				<persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
				<persona-main class="col-10"></persona-main>
			</div>			
			<persona-footer></persona-footer>
		`;
	}    

	newPerson(e) {
		console.log("newPerson en PersonaApp");	
		this.shadowRoot.querySelector("persona-main").showPersonForm = true; 	  	
	}
}
customElements.define('persona-app', PersonaApp)